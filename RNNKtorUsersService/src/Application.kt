package rnn.practice

import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.html.*
import kotlinx.html.*
import io.ktor.content.*
import io.ktor.http.content.*
import io.ktor.gson.*
import io.ktor.features.*
import org.slf4j.event.*
import io.ktor.server.engine.*

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused")
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(ContentNegotiation) {
        gson {
        }
    }

    install(CallLogging) {
        level = Level.INFO
        filter { call -> call.request.path().startsWith("/") }
    }

    install(ShutDownUrl.ApplicationCallFeature) {
        shutDownUrl = this@module.environment.config.property("ktor.deployment.shutdown.url").getString()
        exitCodeSupplier = { 0 }
    }

    routing {
        get("/users") {
            call.respond("Brbrb!")
        }

        static("/static") {
            resources("static")
        }
    }
}

